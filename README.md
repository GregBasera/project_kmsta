# PROJECT
---
| Name | Kmsta |
|------|-------|
| Description | A project assigned to the authors on their Web Development and Programming subject to meassure their understanding of the concepts and the standards of the practice. |
| Authors | Basera, Greg Emerson *gbasera@gbox.adnu.edu.ph* |
|  | Carizo, Ben |
|  | Castaneda, Jan Redentor |
|  | Sia, Justin |

# ENVIRONMENT
---
* HTML
* CSS (Bootstrap)
* JavaScript (JQuery)
* Apache Web Server
* PHP (CodeIgniter)
* MariaDB

# FEATURES
---
##### Kmsta is a Web-based Social Media Application that lets people:
1. **Post statements on a Timeline**
    * Users would be able to contribute a user-defined post on a public timeline accessible by everyone on the network.
2. **Upvote or Downvote a posted statement**
    * Users would be able to upvote or downvote a post.  
3. **Annotate other users' post**
    * Users would be able to select specific text from a post and comment on it. The selected text will then be highlighted for other people to see. Kinda like google docs.

# **DEVELOPER'S** MAKE SURES
---
1. Make sure that Apache is installed in your machine.
    * $ sudo apt update
    * $ sudo apt install apache2
2. Make sure that PHP is also installed in your machine.
    * $ sudo apt-get install php -y
3. Apache config file should be set properly.
    * Change **DocumentRoot /var/www/html** to wherever path you cloned this repository
        * $ sudo nano /etc/apache2/sites-available/000-default.conf
    * Change **<Directory /var/www/html/>** to wherever path you cloned this repository
        * $ sudo nano /etc/apache2/apache2.conf
4. Run, stop, or restart Apache.
    * $ sudo systemctl start apache2
    * $ sudo systemctl stop apache2
    * $ sudo systemctl restart apache2
