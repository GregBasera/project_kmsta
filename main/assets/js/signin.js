console.log("signin.js is loaded");

function strength(){
  var len = document.Signup.SPassword.value.length;

  if(len < 9)
    $('[name="SPassword"]').removeClass('border-success border-warning').addClass('border-danger');
  else if(len > 15)
    $('[name="SPassword"]').removeClass('border-warning border-danger').addClass('border-success');
  else
    $('[name="SPassword"]').removeClass('border-success border-danger').addClass('border-warning');
}

function matched(){
  if(document.Signup.SPassword.value == document.Signup.PasswordRe.value){
    $('[name="PasswordRe"]').removeClass('border-danger').addClass('border-success');
  } else {
    $('[name="PasswordRe"]').removeClass('border-success').addClass('border-danger');
  }
}

function sign_in(){
  var data = {
    fname : document.Signup.Firstname.value,
    lname : document.Signup.Lastname.value,
    email : document.Signup.Email.value,
    agree : document.Signup.Agree.checked,
    username : document.Signup.SUsername.value,
    password : document.Signup.SPassword.value,
    re_pword : document.Signup.PasswordRe.value,
  };

  var formfilled = (data.fname == '' || data.lname == '' || data.email == '' || data.username == '' || data.password == '' || !data.agree) ? false : true;

  if(formfilled && data.re_pword == data.password){
    if($.isNumeric(data.fname) || $.isNumeric(data.lname)){
      $('#signup-alert').removeClass('alert-success').addClass('alert-danger')
      .html("<strong>Does your name really contain numbers?</strong>. Can you try that again?").removeAttr('hidden');
    } else {
      $('#signup-alert').removeClass('alert-danger').addClass('alert-success')
      .html("<strong>Welcome!</strong> Your page is loading, please wait.").removeAttr('hidden');
    }
  } else {
    $('#signup-alert').removeClass('alert-success').addClass('alert-danger')
    .html("<strong>I think you missed something above</strong>. Can you try that again?").removeAttr('hidden');
  }
}
