function fakeClicked(){
  $('#Post-textarea-fake').removeClass('d-block').addClass('d-none');
  $('#Post-textarea-real').removeClass('d-none').addClass('d-block').focus();
  $('#Post-image, #Post-chCounter, #Post-button').removeClass('d-none').addClass('d-block');
  $('#Post-ghost').addClass('navbar');
}

function postFocusRemoved(){
  $('#Post-textarea-real').removeClass('d-block').addClass('d-none').val('');
  $('#Post-textarea-fake').removeClass('d-none').addClass('d-block');
  $('#Post-image, #Post-chCounter, #Post-button').removeClass('d-block').addClass('d-none');
  $('#Post-chCounter').html('0/200');
  $('#Post-ghost').removeClass('navbar');
}

function countCharacters(){
  var ch = document.getElementById('Post-textarea-real').value.length;
  $('#Post-chCounter').html(ch + '/200');
  if(ch > 0)
    $('#Post-button').removeAttr('disabled');
  else
    $('#Post-button').attr('disabled', 'true');
}

function like(object, value){
  object.innerHTML = "<i class='material-icons'>thumb_up</i> " + (parseInt(value) + 1);
  object.value = (parseInt(value) + 1);
}

function selected(obj){
  if(window.getSelection().toString() != ''){
    var selection = window.getSelection().toString();
    alert("This should be annotated: " + selection);
  }
}

function change(){
  var docHeight = $('#ghostArea').height();
  var docWidth = $('#ghostArea').width();
  var divWidth = $('#test').width();
  var divHeight = $('#test').height();
  var heightMax = docHeight - divHeight;
  var widthMax = docWidth - divWidth;

  $('#test').css({
    left: Math.floor( Math.random() * widthMax ),
    top: Math.floor( Math.random() * heightMax )
  });

  console.log($(document).height(), $(document).width());
  console.log(docHeight, docWidth);
  console.log(Math.random() * widthMax, Math.random() * heightMax);
}

console.log('home.js loaded');
