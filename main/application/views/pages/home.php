<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include("application/views/templates/head-CDNs.php"); ?>
    <?php //include("application/views/templates/head-assets.php") ?>

    <!-- Custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/kmsta-general.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/kmsta-home.css">
  </head>

  <body>
    <div class="row wrapper m-0">
      <div class="sidebar col-md-3 d-none d-md-block text-white">
        <?php include("application/views/templates/sidebar.php"); ?>
      </div>

      <div class="mainpane col-md-9 col-sm-12">
        <div class="sticky-top">
          <div class="row navbar">
            <?php include("application/views/templates/navbar.php"); ?>
          </div>
          <div class="row mb-4 post-ghost">
            <div class="col"></div>
            <div class="col-md-7 mx-auto rounded" id="Post-ghost" style="top:-54px;">
              <?php include("application/views/templates/post-set.php"); ?>
            </div>
            <div class="col"></div>
          </div>
        </div>

        <div class="row scrollable">
          <div class="col"></div>
          <div class="col-md-7">
            <?php
              foreach ($posts->result() as $row){
                if($row->media == NULL)
                  include("application/views/templates/post-card-textonly.php");
                else
                  include("application/views/templates/post-card-wmedia.php");
              }
            ?>
          </div>
          <div class="col"></div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/home.js"></script>
  </body>
</html>
