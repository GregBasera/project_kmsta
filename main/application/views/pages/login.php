<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include('application/views/templates/head-CDNs.php'); ?>
    <?php //include('application/views/templates/head-assets.php'); ?>

    <!-- Custom -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/kmsta-general.css">
  </head>

  <body>
    <div class="row m-0 p-0" style="height:100vh;">
      <div class="col-md-6 d-none d-md-block text-white" style="background-color:#337799;">

      </div>
      <div class="col-md-6 col-sm-12">
        <div class="row align-items-center" style="height:10vh">
          <div class="col-2 px-3" style="color:#337799;">
            <h4><b><i>kmsta.</i></b></h4>
          </div>
          <div class="col-10">
            <form class="" target="" action="login" name="Login" method="post">
              <div class="form-row">
                <div class="col-5">
                  <input class="form-control" type="text" name="Username" placeholder="Username" value="" required>
                </div>
                <div class="col-5">
                  <input class="form-control" type="password" name="Password" placeholder="Password" value="" required>
                </div>
                <div class="col-2">
                  <button class="btn btn-block kmsta-button" type="button" name="login" onclick="log_in()">Log in</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="cotainer-fluid" style="height:10vh">
          <div class="alert alert-dismissible fade show" role="alert" hidden id="login-alert"></div>
        </div>

        <div class="row align-items-center" style="height:80vh">
          <div class="col p-5">
            <form class="" action="index.html" name="Signup" method="post">
              <div class="form-row form-group">
                <div class="col-6">
                  <input class="form-control" type="text" name="Firstname" placeholder="Firstname" value="">
                </div>
                <div class="col-6">
                  <input class="form-control" type="text" name="Lastname" placeholder="Lastname" value="">
                </div>
              </div>
              <div class="row form-group">
                <div class="col">
                  <input class="form-control" type="email" name="Email" placeholder="Email" value="">
                </div>
              </div>
              <div class="row form-group">
                <div class="col">
                  <input class="form-control" type="text" name="SUsername" placeholder="Username" value="">
                </div>
              </div>
              <div class="row form-group">
                <div class="col">
                  <input class="form-control" type="password" name="SPassword" placeholder="Password" value="" onkeyup="strength()">
                </div>
              </div>
              <div class="row form-group">
                <div class="col">
                  <input class="form-control" type="password" name="PasswordRe" placeholder="Confirm Password" value="" onkeyup="matched()">
                </div>
              </div>
              <div class="form-row form-group">
                <div class="col-8">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="Agree" id="Agree" value="yes">
                    <label class="form-check-label text-muted" for="Agree">I agree to the terms and conditions of this site</label>
                  </div>
                </div>
                <div class="col-4">
                  <button class="btn btn-block kmsta-button" type="button" name="Signup" onclick="sign_in()">Sign up</button>
                </div>
              </div>
              <div class="container-fluid" style="height:10vh">
                <div class="alert alert-dismissible fade show" role="alert" hidden id="signup-alert"></div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/login.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/signin.js"></script>
  </body>
</html>
