<div class="row">
  <div class="col-12 c_pic text-white" style="background-color:#337799;">
    <!-- this should be an image -->
  </div>
</div>
<div class="row py-2" style="background-color:#115566">
  <div class="col-4">
    <!-- this is just a sample.. real picture will come from database -->
    <img height="88px" width="88px" class="rounded p_pic" src="<?php echo base_url(); ?>assets/img/ProfilePicture-squared.png" alt="">
  </div>
  <div class="col-8">
    <h4>Username</h4>
    <h6 class="">@Username</h6>
  </div>
</div>
<div class="row pb-2" style="background-color:#115566">
  <div class="col-4 text-center">
    <h6>Post</h6>
    <h5>999</h5>
  </div>
  <div class="col-4 text-center">
    <h6>Followers</h6>
    <h5>999</h5>
  </div>
  <div class="col-4 text-center">
    <h6>Following</h6>
    <h5>999</h5>
  </div>
</div>
<div class="row py-2">
  <div class="col-12 px-4 text-justify" id="ghostArea">
    <div id="test" style="position:absolute" onclick="change();">div</div>
  </div>
</div>
