<div class="jumbotron jumbotron-fluid py-3 rounded">
  <div class="row m-0">
    <div class="col d-flex align-items-center justify-content-start">
      <!-- this is just a sample.. real picture will come from database -->
      <img height="43px" width="43px" class="rounded mr-2" src="<?php echo base_url(); ?>assets/img/ProfilePicture-squared.png" alt="">
      <div class="">
        <h6>Username</h6>
        <small class="text-muted">@username</small>
      </div>
    </div>
    <!-- the value attribute of these buttons must represent the amount of interactions -->
    <div class="col d-flex align-items-center justify-content-end">
      <button class="btn kmsta-discrete-button mx-1" type="button" onclick="like(this, value)" value="99" name="Upvote">
        <i class="material-icons">thumb_up</i> 99
      </button>
    </div>
  </div>
  <div class="row m-0 p-0">
    <div class="col-12 mt-3 px-4 post-text" onmouseup="selected(this)">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
    <div class="col-12 mt-3 d-flex justify-content-center">
      <!-- this is just a sample.. real picture will come from database -->
      <img class="rounded img-responsive" src="<?php echo base_url(); ?>assets/img/36231833334_b3581aa9af_o.png" alt="">
    </div>
  </div>
</div>
