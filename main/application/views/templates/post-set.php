<textarea class="form-control d-none mx-2" maxlength="200" id="Post-textarea-real" value="" placeholder="Musta? nuginagawamu?" onblur="postFocusRemoved()" onkeyup="countCharacters()" style="height:114px"></textarea>
<div class="container">
  <div class="d-flex align-items-center justify-content-start">
    <button class="btn kmsta-button text-white mt-2 d-none" type="button" id="Post-image">
      <i class="material-icons">image</i> Image
    </button>
  </div>
  <div class="d-flex align-items-center justify-content-end">
    <b class="text-light mt-2 mx-2 d-none" id="Post-chCounter">0/200</b>
    <button class="btn kmsta-button mt-2 d-none" type="button" id="Post-button" onclick="postFocusRemoved()" disabled>
      <i class="material-icons">send</i> Post
    </button>
  </div>
</div>
