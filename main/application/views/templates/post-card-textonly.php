<div class="jumbotron jumbotron-fluid py-3 rounded">
  <div class="row m-0">
    <div class="col d-flex align-items-center justify-content-start">
      <!-- this is just a sample.. real picture will come from database -->
      <img height="43px" width="43px" class="rounded mr-2" src="<?php echo base_url(); ?>assets/img/ProfilePicture-squared.png" alt="">
      <div class="container pl-0">
        <h6><b><?php echo $row->user_tag; ?></b></h6>
        <small class="text-muted">@<?php echo $row->username; ?></small>
      </div>
    </div>
    <!-- the value attribute of these buttons must represent the amount of interactions -->
    <div class="col d-flex align-items-center justify-content-end">
      <button class="btn kmsta-discrete-button mx-1" type="button" onclick="like(this, value)" value="<?php echo $row->likes; ?>" name="Upvote">
        <i class="material-icons">thumb_up</i> <?php echo $row->likes; ?>
      </button>
    </div>
  </div>
  <div class="container mt-3 px-4 post-text" onmouseup="selected(this)">
    <?php echo $row->body; ?>
  </div>
  <div class="container text-muted d-flex justify-content-end">
    <small><?php echo $row->date_created; ?></small>
  </div>
</div>
