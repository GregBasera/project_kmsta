<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title ?></title>

<!-- Stylesheets -->
<!-- Essentials -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- In assets -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-4.2.1-dist/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-4.2.1-dist/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-4.2.1-dist/css/bootstrap.min.css">

<!-- Javascript -->
<!-- In assets -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-4.2.1-dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-4.2.1-dist/js/bootstrap.min.js"></script>
