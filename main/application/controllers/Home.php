<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$data['title'] = 'Home';
		$data['posts'] = $this->db->query('CALL getPosts();');

		$this->load->view('pages/home', $data);
	}
}
