CREATE DATABASE IF NOT EXISTS KMSTA;

use KMSTA;

CREATE TABLE User_Profiles (
  user_ID     varchar(15)      not null,
  user_tag    varchar(50)      not null,
  email       varchar(70)      not null,
  username    varchar(30)      not null,
  password    text             not null,
  bio         varchar(256),
  cover_pic   text,
  profile_pic text,

  constraint primary key (user_ID)
);

CREATE TABLE Follows (
  user_ID       varchar(15)      not null,
  following     varchar(15)      not null,

  constraint primary key (user_ID, following),
  constraint foreign key (user_ID) REFERENCES User_Profiles (user_ID),
  constraint foreign key (following) REFERENCES User_Profiles (user_ID)
);

CREATE TABLE Posts (
  post_ID       varchar(15)      not null,
  user_ID       varchar(15)      not null,
  date_created  date             not null,
  body          varchar(256)     not null,
  media         text,
  likes         int unsigned     default 0,

  constraint primary key (post_ID),
  constraint foreign key (user_ID) REFERENCES User_Profiles (user_ID)
);

CREATE TABLE Annotations (
  post_ID       varchar(15)      not null,
  user_ID       varchar(15)      not null,
  annot_word    varchar(20)      not null,
  body          varchar(256)     not null,

  constraint primary key (post_ID, user_ID),
  constraint foreign key (post_ID) REFERENCES Posts (post_ID),
  constraint foreign key (user_ID) REFERENCES User_Profiles (user_ID)
);

-- the php function 'uniqid()' will be used to generate user_ID
-- the php function 'sha1()' will be used to encrypt password
INSERT INTO User_Profiles (user_ID, user_tag, email, username, password, bio) VALUES
('5c714d50f1bad', 'JustaDumbDude', 'gbasera@gbox.adnu.edu.ph', 'Fudge', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'First KMSTA profile.'),
('5c714f1c5e33b', 'BenDover', 'bend@gmail.com', 'BenD', 'bfb7759a67daeb65410490b4d98bb9da7d1ea2ce', 'Second KMSTA profile');

INSERT INTO Follows (user_ID, following) VALUES
('5c714d50f1bad', '5c714f1c5e33b'),
('5c714f1c5e33b', '5c714d50f1bad');

-- the php function 'uniqid()' will be used to generate post_ID
-- the php function 'date("Y-m-d")' will be used to generate date_created
INSERT INTO Posts (post_ID, user_ID, date_created, body) VALUES
('5c7154cae3c4b', '5c714d50f1bad', '2019-02-22', 'This is from database'),
('5c71557f392fb', '5c714f1c5e33b', '2019-02-23', 'Hello KMSTA!');

-- annot_word should be a word inside any one of posts
INSERT INTO Annotations (post_ID, user_ID, annot_word, body) VALUES
('5c7154cae3c4b', '5c714f1c5e33b', 'only', 'No, you are wrong..'),
('5c71557f392fb', '5c714d50f1bad', 'KMSTA', 'KMSTA said "hi"...');

DROP PROCEDURE IF EXISTS getPosts;
DELIMITER //
CREATE PROCEDURE getPosts()
BEGIN
  select up.user_tag, up.username, up.user_ID, p.post_ID, p.date_created, p.body, p.media, p.likes
  from User_Profiles up, Posts p
  where up.user_ID = p.user_ID;
END; //
DELIMITER ;

COMMIT;
